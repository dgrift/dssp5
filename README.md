# DSSP5: do-it-yourself SELinux crafting kit for GNU/Linux distributions
*if you can dream it you can do it*

One size fits no one. When it comes to enforcing meaningful fine-grained mandatory access control this is the dilemma we are facing. Either it is too loose or it is too tight. It does not help if adjusting the configuration is hard and has limitations. Flexible SELinux is so much more fun when you are in the driver seat.

## The canvas

Writing SELinux policy is a lot like creating artwork with for example Inkscape. Your piece is a stack of layers. DSSP5 aims to be the first few layers in this stack and provides rough outlines by creating low level partitions of your GNU/Linux system. You can refine your work further by stacking layers on top and inheriting templates depending on what matters to you.

## The paint

DSSP5 leverages a modern policy language that is native to SELinux called Common Intermediate Language that facilitates stacking with abstractions such as templates, macros, class permissions and class maps. These abstractions are added to the first layers so that you can take advantage of them by inheriting them all the way to the top of your stack. The most common colors used in a painting are already there for you to use.

## The brushes

Writing SELinux policy is a cycle of planning, designing, writing, deploying and testing. It helps if the path from start to finish is short. When you have an idea then you want to be able to try it out with little hassle. DSSP5 enables you to add policy and modify every aspect of the lower layers locally and at runtime. Your changes take precedence and all the sources are at your fingertips in the SELinux module store. These sources are human readible and can be imported and exported. All you need is the semodule command and your favorite text editor. There is no building of sources involved.

In practice this all means that once you have installed DSSP5 you can start creating in your own pace and at runtime. DSSP5 is not likely to get in your way. Take advantage of it where you want to and override it where you have to. You will have SELinux enforcing from the moment you installed DSSP5 and it is unlikely that you will notice at least until you start adding your own rules.

* Leverages modern CIL features
* Templates from the ground up
* Human readable policy all the way
* All you need is semodule and your favorite text editor
* Ability to override every single aspect of DSSP5 at runtime most of the time
* Your additions and customizations take precedence
* Works by default in enforcing mode
* Create on-the-fly

DSSP5 takes care of the boring work for you. You get to do the interesting part.

# Dependencies

`apt-get install policycoreutils findutils make --no-install-recommends`

# Suggests

`apt-get install setools --no-install-recommends`

# Installation

`make modular_install`

```
cat > /etc/selinux/config <<'EOF'
SELINUX=enforcing
SELINUXTYPE=dssp5
EOF
```

Append `security=selinux` to the kernel command line.

`echo '-FM' > /.autorelabel && reboot`

*Labeling is the most tricky part of SELinux and if you use `Btrfs` with subvolumes then you may want to pay special attention to it*

You can find any unlabeled and mislabeled content by running:

* `find / -context sys.id:sys.role:unlabeled:s0`
* `find / -context sys.id:sys.role:invalid:s0`

# Tips

You can export the whole of DSSP5:

`for module in $(semodule -l); do semodule -E $module ; done`

Edit any one of them then load it back into the module store. Your version will take precedence:

`echo ';; hello world' >> conf.cil && semodule -i conf.cil`

Start adding your policy:

* `echo '(type myfirsttype)' > myfirsttype.cil && semodule -i myfirsttype.cil`
* `seinfo -t myfirsttype`

The DSSP5 `misc.cil` module is used to correspond GNU/Linux aspects with Debian specifics. For example it associates the `sys.id:sys.role:conf.file:s0` label with `/etc` because DSSP5 aims to be easily adjustable to any GNU/Linux distribution. It is just that Debian is used as the development platform for DSSP5 and `misc.cil` is what glues the two together.

**The secret to success is planning. Work in small self contained increments. That way you should be able to end the day with SELinux enforcing just as you started the day.**

# Handy documentation and support

* `man semodule`
* https://github.com/SELinuxProject/selinux/blob/main/secilc/docs/README.md
* https://github.com/SELinuxProject/selinux-notebook
* #selinux on irc.libera.chat

# Easier said than done

I eat my own dog food and built a personalized SELinux security policy on top of DSSP5 that can be used for reference:

* https://git.defensec.nl/?p=dssp5.git;a=tree;h=refs/heads/dssp5-debian;hb=refs/heads/dssp5-debian
